libmiglayout-java (11.4.2+ds-2) unstable; urgency=medium

  * Fixing unversioned symlink for the global jar in /usr/share/java
    (Closes: #1094964)
  * Removing useless B-D on ant

 -- Pierre Gruet <pgt@debian.org>  Sat, 01 Feb 2025 23:25:46 +0100

libmiglayout-java (11.4.2+ds-1) unstable; urgency=medium

  * New upstream version 11.4.2+ds
  * Refreshing patches
  * Raising Standards version to 4.7.0 (no change)
  * Marking the -java package as Multi-Arch: foreign
  * Refreshing d/libmiglayout-java.poms after the ideutil got removed upstream
  * Skipping annotations with error_prone
  * Omitting jreleaser Maven plugin

 -- Pierre Gruet <pgt@debian.org>  Wed, 09 Oct 2024 15:21:38 +0200

libmiglayout-java (11.1+ds-1) unstable; urgency=medium

  * New upstream version 11.1+ds
  * Refreshing patches
  * Raising Standards version to 4.6.2:
    - Using Copyright-Format 1.0
  * Repacking through d/copyright
  * Building with javahelper to set the classpath of the built jar
  * Updating Maven ignoreRules
  * Adding myself as uploader
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Adding a Lintian override for files with very-long-lines

 -- Pierre Gruet <pgt@debian.org>  Tue, 20 Jun 2023 21:36:44 +0200

libmiglayout-java (5.1-3) unstable; urgency=medium

  [ Pierre Gruet ]
  * Team upload.
  * Set Standards version to 4.5.1 (Rules-Requires-Root: no)
  * Update Vcs- fields in d/control
  * Removing useless scripts for package update
  * Depending on debhelper-compat 13
  * Putting a default Maven dependency on org.eclipse.swt (Closes: #976546)

  [ Markus Koschany ]
  * Add javafx.patch
  * Add a Maven rule for javafx-base.
  * Add javafx.patch
  * Add a Maven rule for javafx-base.

 -- Pierre Gruet <pgtdebian@free.fr>  Thu, 31 Dec 2020 17:57:00 +0100

libmiglayout-java (5.1-2) unstable; urgency=medium

  * Team upload.
  * Declare compliance with Debian Policy 4.2.1.
  * Build the javafx module.
  * Ignore test dependencies for javafx module.
  * Build-depend on libopenjfx-java.
  * Add javafx.patch and depend on the new OpenJFX11 artifacts.

 -- Markus Koschany <apo@debian.org>  Sun, 07 Oct 2018 11:21:45 +0200

libmiglayout-java (5.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 5.1.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.2.0.
  * Update debian/watch and check for updates at github.com.

 -- Markus Koschany <apo@debian.org>  Sat, 11 Aug 2018 21:28:01 +0200

libmiglayout-java (4.2-3) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure on i386 (Closes: #860632)

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 19 Apr 2017 13:09:42 +0200

libmiglayout-java (4.2-2) unstable; urgency=medium

  * Team upload.
  * Transition to libswt-gtk-4-java
  * Build with the DH sequencer instead of CDBS
  * Moved the package to Git
  * Standards-Version updated to 3.9.8

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 01 Aug 2016 23:09:56 +0200

libmiglayout-java (4.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Build with maven-debian-helper and install the Maven artifacts
  * Download the new releases from code.google.com
  * debian/control:
    - Added a suggested dependency on libswt-gtk-3-java
    - Removed the build dependency on substance
    - Standards-Version updated to 3.9.5 (no changes)
    - Use canonical URLs for the Vcs-* fields
  * Switch to debhelper level 9
  * Use XZ compression for the upstream tarball

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 11 Mar 2014 09:41:19 +0100

libmiglayout-java (3.7.4-2) unstable; urgency=low

  * Replace libswt-gtk-3.6-java with libswt-gtk-3-java

 -- Andrew Ross <ubuntu@rossfamily.co.uk>  Wed, 27 Jul 2011 23:35:34 +0100

libmiglayout-java (3.7.4-1) unstable; urgency=low

  * New upstream release.
  * Replace libswt-gtk-3.5-java with libswt-gtk-3.6-java (Closes: #629721)
  * Bumped standards version to 3.9.2 (no changes)
  * Drop dependency on jre.
  * Added debian/source/format file
  * Removed reference to /usr/share/common-licenses/BSD as it is no
    longer included.

 -- Andrew Ross <ubuntu@rossfamily.co.uk>  Wed, 08 Jun 2011 19:27:32 +0100

libmiglayout-java (3.7.2-1) unstable; urgency=low

  * New upstream release
  * debian/control: replace libswt-gtk-3.4-java with libswt-gtk-3.5-java,
    fixes build (Closes: #564387)

 -- Varun Hiremath <varun@debian.org>  Sun, 24 Jan 2010 13:59:12 -0500

libmiglayout-java (3.7.1-2) unstable; urgency=low

  * debian/rules: DEB_JARS use swt.jar provided using alternatives in
    /usr/share/java instead of the versioned jar in /usr/lib/java

 -- Varun Hiremath <varun@debian.org>  Mon, 31 Aug 2009 19:50:21 -0400

libmiglayout-java (3.7.1-1) unstable; urgency=low

  * New upstream release
  * Build-Depends: Replace libswt3.2-gtk-java with libswt-gtk-3.4-java,
    fixes FTBFS (Closes: #543129)
  * Bump Standards-Version to 3.8.3

 -- Varun Hiremath <varun@debian.org>  Mon, 24 Aug 2009 03:10:26 -0400

libmiglayout-java (3.7-1) unstable; urgency=low

  * Initial release (Closes: #530490)

 -- Varun Hiremath <varun@debian.org>  Mon, 25 May 2009 01:59:26 -0400
